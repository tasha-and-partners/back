<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PreferredTopicController;
use App\Http\Controllers\API\QuestionAnswerController;
use App\Http\Controllers\API\SurveyController;
use App\Http\Controllers\API\SurveyQuestionController;
use App\Http\Controllers\API\TopicController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\WorkshopController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register'])
    ->name('register');

Route::post('/admin-login', [AuthController::class, 'adminLogin'])
    ->name('admin-login');

Route::post('/login', [AuthController::class, 'login'])
    ->name('login');

Route::post('/logout', [AuthController::class, 'logout'])
    ->middleware('auth:sanctum');

Route::apiResource('/users', UserController::class)
    ->middleware('auth:sanctum');
Route::post('/users/{id}', [UserController::class, 'update'])
    ->middleware('auth:sanctum');

Route::apiResource('/topics', TopicController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/preferred-topics', PreferredTopicController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/workshops', WorkshopController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/surveys', SurveyController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/survey-questions', SurveyQuestionController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/question-answers', QuestionAnswerController::class)
    ->middleware('auth:sanctum');
