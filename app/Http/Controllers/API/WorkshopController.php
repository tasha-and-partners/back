<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Workshop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $workshops = Workshop::orderBy('updated_at', 'desc')->get();

            Log::info('Workshops searched', [
                'workshops' => $workshops
            ]);

            return response()->json($workshops);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for workshops', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'speaker_id' => ['required', 'numeric', 'min:1'],
                'topic_id' => ['required', 'numeric', 'min:1'],
                'title' => ['required', 'string', 'max:255'],
                'outline' => ['required', 'string'],
                'session' => ['required', Rule::in(['morning', 'afternoon', 'closing'])]
            ]);

            if ($validator->fails()) {
                Log::warning('Saving workshop validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $speakerId = $request->speaker_id;
            $topicId = $request->topic_id;
            $title = $request->title;
            $outline = $request->outline;
            $session = $request->session;

            // Populate a workshop object
            $workshop = new Workshop([
                'speaker_id' => $speakerId,
                'topic_id' => $topicId,
                'title' => $title,
                'outline' => $outline,
                'session' => $session
            ]);

            // Save the workshop and return a message
            if ($workshop->save()) {
                Log::info('Workshop saved successfully', ['workshop_id' => $workshop->id]);

                return response()->json([
                    'message' => __('Workshop saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the workshop');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the workshop')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the workshop', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving workshop by ID', ['workshop_id' => $id]);

            return response()->json(Workshop::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve workshop by ID', [
                'workshop_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'speaker_id' => ['required', 'numeric', 'min:1'],
                'topic_id' => ['required', 'numeric', 'min:1'],
                'title' => ['required', 'string', 'max:255'],
                'outline' => ['required', 'string'],
                'session' => ['required', Rule::in(['morning', 'afternoon', 'closing'])]
            ]);

            if ($validator->fails()) {
                Log::warning('Updating workshop validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $speakerId = $request->speaker_id;
            $topicId = $request->topic_id;
            $title = $request->title;
            $outline = $request->outline;
            $session = $request->session;

            // Update workshop
            $affected = Workshop::where('id', $id)
                ->update([
                    'speaker_id' => $speakerId,
                    'topic_id' => $topicId,
                    'title' => $title,
                    'outline' => $outline,
                    'session' => $session
                ]);

            if ($affected) {
                Log::info('Workshop updated successfully.', ['workshop_id' => $id]);

                return response()->json(Workshop::find($id));
            }

            Log::error('An error occurred when updating the workshop');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the workshop')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the workshop', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the workshop
            $workshop = Workshop::find($id);

            // Delete the workshop
            $workshop->delete();

            if ($workshop->trashed()) {
                Log::info('Workshop deleted successfully', ['workshop_id' => $id]);

                return response()->json([
                    'message' => __('Workshop deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the workshop');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the workshop')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the workshop', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
