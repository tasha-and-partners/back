<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\AccountCreated;
use App\Models\Moderator;
use App\Models\Speaker;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $username = $request->query('name');
            $role = $request->query('role');

            $users = User::where(function ($query) use ($username) {
                if ($username != null) {
                    return $query->where('first_name', 'LIKE', '%' . $username . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $username . '%')
                        ->orWhere('middle_name', 'LIKE', '%' . $username . '%');
                }
            })
            ->where(function ($query) use ($role) {
                if ($role != null) {
                    return $query->where('role', $role);
                }
            })
            ->orderBy('first_name', 'asc')
            ->orderBy('last_name', 'asc')
            ->get();

            Log::info('Users searched', [
                'users' => $users,
                'searched_name' => $username,
                'searched_role' => $role
            ]);

            return response()->json($users);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for users', [
                'error' => $e,
                'searched_name' => $username,
                'searched_role' => $role
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'role' => ['required', Rule::in(['admin', 'subscriber', 'speaker', 'moderator'])],
                'email' => ['required', 'unique:users', 'email:rfc,dns'],
                'phone' => ['required', 'unique:users', 'string', 'max:9']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving user validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $middleName = $request->middle_name;
            $role = $request->role;
            $email = $request->email;
            $phone = $request->phone;
            $password = Str::random(8);

            // Populate a user object
            $user = new User([
                'first_name' => $firstName,
                'middle_name' => $middleName,
                'last_name' => $lastName,
                'email' => $email,
                'avatar' => '',
                'role' => $role,
                'phone' => $phone,
                'password' => Hash::make($password)
            ]);

            // Save the user and return a message
            if ($user->save()) {
                Log::info('User saved successfully', ['user_id' => $user->id]);

                if ($role == 'speaker') {
                    Log::info('Creating speaker during user create.', ['user_id' => $user->id]);

                    // Create speaker
                    $cv = '';
                    if ($request->hasFile('cv') && $request->file('cv')->isValid()) {
                        $cv = $request->cv->store('users/cvs');
                    }

                    $speaker = new Speaker([
                        'user_id' => $user->id,
                        'biography' => $request->biography,
                        'cv' => $cv,
                        'themes' => $request->themes,
                        'type' => $request->type
                    ]);

                    $speaker->save();

                    Log::info('Created speaker during user create.', ['user_id' => $user->id]);
                } else if ($role == 'moderator') {
                    Log::info('Creating moderator during user create.', ['user_id' => $user->id]);

                    // Create moderator
                    $moderator = new Moderator([
                        'user_id' => $user->id,
                        'workshop_id' => $request->workshop_id
                    ]);

                    $moderator->save();

                    Log::info('Created moderator during user create.', ['user_id' => $user->id]);
                }

                // Send email to user with their password
                Mail::to($email)->send(new AccountCreated($firstName, $password));

                return response()->json([
                    'message' => __('User saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the user');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving user by ID', ['user_id' => $id]);

            return response()->json(User::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve user by ID', [
                'user_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'avatar' => ['nullable', 'image', 'max:10000'],
                'role' => ['required', Rule::in(['admin', 'subscriber', 'speaker', 'moderator'])],
                'email' => ['required', 'email:rfc,dns'],
                'phone' => ['required', 'string', 'max:9']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating user validation request failed.', [
                    'user_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $firstName = $request->first_name;
            $middleName = $request->middle_name;
            $lastName = $request->last_name;
            $avatar = '';
            if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                $avatar = $request->avatar->store('users/avatars');
            }
            $role = $request->role;
            $email = $request->email;
            $phone = $request->phone;

            // Update user
            $user = User::find($id);

            if ($user->role != $role && ($user->role == 'speaker' || $user->role == 'moderator')) {
                if ($user->role == 'speaker') {
                    Log::info('Deleting speaker during user update.', ['user_id' => $id]);

                    // Delete the speaker
                    $speaker = $user->speaker;

                    $speaker->delete();

                    Log::info('Deleted speaker during user update.', ['user_id' => $id]);
                } else if ($user->role == 'moderator') {
                    Log::info('Deleting moderator during user update.', ['user_id' => $id]);

                    // Delete the moderator
                    $moderator = $user->moderator;

                    $moderator->delete();

                    Log::info('Deleted moderator during user update.', ['user_id' => $id]);
                }
            }

            $user->first_name = $firstName;
            $user->middle_name = $middleName;
            $user->last_name = $lastName;
            $user->avatar = $avatar;
            $user->role = $role;
            $user->email = $email;
            $user->phone = $phone;

            if ($user->save()) {
                Log::info('User updated successfully.', ['user_id' => $id]);

                if ($role == 'speaker') {
                    Log::info('Updating speaker during user update.', ['user_id' => $user->id]);

                    // Create or update speaker
                    $cv = '';
                    if ($request->hasFile('cv') && $request->file('cv')->isValid()) {
                        $cv = $request->cv->store('users/cvs');
                    }

                    $speaker = Speaker::updateOrCreate(
                        ['user_id' => $user->id],
                        [
                            'biography' => $request->biography,
                            'cv' => $cv,
                            'themes' => $request->themes,
                            'type' => $request->type
                        ]
                    );

                    Log::info('Updated speaker during user update.', ['user_id' => $user->id]);
                } else if ($role == 'moderator') {
                    Log::info('Updating moderator during user update.', ['user_id' => $user->id]);

                    // Create or update moderator
                    $moderator = Moderator::updateOrCreate(
                        ['user_id' => $user->id],
                        ['workshop_id' => $request->workshop_id]
                    );

                    Log::info('Updated moderator during user update.', ['user_id' => $user->id]);
                }

                return response()->json(User::find($id));
            }

            Log::error('An error occurred when updating the user');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the user
            $user = User::find($id);

            // Delete the user
            $user->delete();

            if ($user->trashed()) {
                $user->forceDelete();

                Log::info('User deleted successfully', ['user_id' => $id]);

                if ($user->role == 'speaker') {
                    Log::info('Deleting speaker during user delete.', ['user_id' => $id]);

                    $speaker = $user->speaker;

                    $speaker->delete();
                    $speaker->forceDelete();

                    Log::info('Deleted speaker during user delete.', ['user_id' => $id]);
                } else if ($user->role == 'moderator') {
                    Log::info('Deleting moderator during user delete.', ['user_id' => $id]);

                    $moderator = $user->moderator;

                    $moderator->delete();
                    $moderator->forceDelete();

                    Log::info('Deleted moderator during user delete.', ['user_id' => $id]);
                }

                return response()->json([
                    'message' => __('User deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the user');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
