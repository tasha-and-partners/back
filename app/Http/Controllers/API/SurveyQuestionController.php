<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\SurveyQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SurveyQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $searchKey = $request->query('key');

            $questions = SurveyQuestion::where(function ($query) use ($searchKey) {
                if ($searchKey != null) {
                    return $query->where('survey_id', $searchKey);
                }
            })
                ->orderBy('number', 'asc')
                ->get();

            Log::info('Survey questions searched', ['survey_questions' => $questions]);

            return response()->json($questions);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for survey questions', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'survey_id' => ['required', 'numeric', 'min:1'],
                'number' => ['required', 'numeric', 'min:1'],
                'content' => ['required', 'string'],
                'type' => ['required', Rule::in(['mcq', 'yes/no', 'free_input'])],
                'choices' => ['nullable', 'string']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving survey question validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $surveyId = $request->survey_id;
            $number = $request->number;
            $content = $request->content;
            $type = $request->type;
            $choices = $request->choices;

            // Populate a survey question object
            $question = new SurveyQuestion([
                'survey_id' => $surveyId,
                'number' => $number,
                'content' => $content,
                'type' => $type,
                'choices' => $choices
            ]);

            // Save the survey question and return a message
            if ($question->save()) {
                Log::info('Survey question saved successfully', ['survey_question_id' => $question->id]);

                return response()->json([
                    'message' => __('Survey question saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the survey question');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the survey question')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the survey question', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving survey question by ID', ['survey_question_id' => $id]);

            return response()->json(SurveyQuestion::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve survey question by ID', [
                'survey_question_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'survey_id' => ['required', 'numeric', 'min:1'],
                'number' => ['required', 'numeric', 'min:1'],
                'content' => ['required', 'string'],
                'type' => ['required', Rule::in(['mcq', 'yes/no', 'free_input'])],
                'choices' => ['nullable', 'string']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating survey question validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $surveyId = $request->survey_id;
            $number = $request->number;
            $content = $request->content;
            $type = $request->type;
            $choices = $request->choices;

            // Update survey question
            $affected = SurveyQuestion::where('id', $id)
                ->update([
                    'survey_id' => $surveyId,
                    'number' => $number,
                    'content' => $content,
                    'type' => $type,
                    'choices' => $choices
                ]);

            if ($affected) {
                Log::info('Survey question updated successfully.', ['survey_question_id' => $id]);

                return response()->json(SurveyQuestion::find($id));
            }

            Log::error('An error occurred when updating the survey question');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the survey question')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the survey question', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the survey question
            $question = SurveyQuestion::find($id);

            // Delete the survey question
            $question->delete();

            if ($question->trashed()) {
                Log::info('Survey question deleted successfully', ['survey_question_id' => $id]);

                return response()->json([
                    'message' => __('Survey question deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the survey question');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the survey question')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the survey question', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
