<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\QuestionAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class QuestionAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $searchKey = $request->query('key');

            $answers = QuestionAnswer::where(function ($query) use ($searchKey) {
                if ($searchKey != null) {
                    return $query->where('survey_question_id', $searchKey);
                }
            })
            ->get();

            Log::info('Survey question answers searched', [
                'surveys' => $answers,
                'search_key' => $searchKey
            ]);

            return response()->json($answers);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for survey question answers', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'survey_question_id' => ['required', 'numeric', 'min:1'],
                'user_id' => ['required', 'numeric', 'min:1'],
                'content' => ['required', 'string']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving survey question answer validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $surveyQuestionId = $request->survey_question_id;
            $userId = $request->user_id;
            $content = $request->content;

            // Populate a question answer object
            $answer = new QuestionAnswer([
                'survey_question_id' => $surveyQuestionId,
                'user_id' => $userId,
                'content' => $content
            ]);

            // Save the question answer and return a message
            if ($answer->save()) {
                Log::info('Survey question answer saved successfully', ['question_answer_id' => $answer->id]);

                return response()->json([
                    'message' => __('Survey question answer saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the survey question answer');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the survey question answer')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the survey question answer', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving survey question answer by ID', ['question_answer_id' => $id]);

            return response()->json(QuestionAnswer::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve survey question answer by ID', [
                'question_answer_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'survey_question_id' => ['required', 'numeric', 'min:1'],
                'user_id' => ['required', 'numeric', 'min:1'],
                'content' => ['required', 'string']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating survey question answer validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $surveyQuestionId = $request->survey_question_id;
            $userId = $request->user_id;
            $content = $request->content;

            // Update question answer
            $affected = QuestionAnswer::where('id', $id)
                ->update([
                    'survey_question_id' => $surveyQuestionId,
                    'user_id' => $userId,
                    'content' => $content
                ]);

            if ($affected) {
                Log::info('Survey question answer updated successfully.', ['question_answer_id' => $id]);

                return response()->json(QuestionAnswer::find($id));
            }

            Log::error('An error occurred when updating the survey question answer');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the survey question answer')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the survey question answer', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the question answer
            $answer = QuestionAnswer::find($id);

            // Delete the question answer
            $answer->delete();

            if ($answer->trashed()) {
                Log::info('Survey question answer deleted successfully', ['question_answer_id' => $id]);

                return response()->json([
                    'message' => __('Survey question answer deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the survey question answer');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the survey question answer')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the survey question answer', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
