<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $searchKey = $request->query('key');

            $surveys = Survey::where(function ($query) use ($searchKey) {
                if ($searchKey != null) {
                    return $query->where('title', 'LIKE', '%' . $searchKey . '%');
                }
            })
            ->orderBy('updated_at', 'desc')
            ->get();

            Log::info('Surveys searched', [
                'surveys' => $surveys,
                'search_key' => $searchKey
            ]);

            return response()->json($surveys);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for surveys', [
                'error' => $e,
                'search_key' => $searchKey
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'workshop_id' => ['required', 'numeric', 'min:1'],
                'title' => ['required', 'string', 'max:255'],
                'description' => ['nullable', 'string'],
                'status' => ['required', Rule::in(['0', '1'])]
            ]);

            if ($validator->fails()) {
                Log::warning('Saving survey validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $workshopId = $request->workshop_id;
            $title = $request->title;
            $description = $request->description;
            $status = $request->status;

            // Populate a survey object
            $survey = new Survey([
                'workshop_id' => $workshopId,
                'title' => $title,
                'description' => $description,
                'status' => $status
            ]);

            // Save the survey and return a message
            if ($survey->save()) {
                Log::info('Survey saved successfully', ['survey_id' => $survey->id]);

                return response()->json([
                    'message' => __('Survey saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the survey');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the survey')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the survey', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving survey by ID', ['survey_id' => $id]);

            return response()->json(Survey::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve survey by ID', [
                'survey_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'workshop_id' => ['required', 'numeric', 'min:1'],
                'title' => ['required', 'string', 'max:255'],
                'description' => ['nullable', 'string'],
                'status' => ['required', Rule::in(['0', '1'])]
            ]);

            if ($validator->fails()) {
                Log::warning('Updating survey validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $workshopId = $request->workshop_id;
            $title = $request->title;
            $description = $request->description;
            $status = $request->status;

            // Update survey
            $affected = Survey::where('id', $id)
                ->update([
                    'workshop_id' => $workshopId,
                    'title' => $title,
                    'description' => $description,
                    'status' => $status
                ]);

            if ($affected) {
                Log::info('Survey updated successfully.', ['survey_id' => $id]);

                return response()->json(Survey::find($id));
            }

            Log::error('An error occurred when updating the survey');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the survey')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the survey', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the survey
            $survey = Survey::find($id);

            // Delete the survey
            $survey->delete();

            if ($survey->trashed()) {
                Log::info('Survey deleted successfully', ['survey_id' => $id]);

                return response()->json([
                    'message' => __('Survey deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the survey');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the survey')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the survey', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
