<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\PreferredTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PreferredTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $preferred = PreferredTopic::where('user_id', $request->user()->id)
                ->get();

            Log::info('Preferred topics searched', ['preferred_topics' => $preferred]);

            return response()->json($preferred);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for preferred topics', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'topic_id' => ['required', 'numeric', 'min:1'],
                'ranking' => ['required', 'numeric', 'min: 1', 'max:5']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving preferred topic validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $topicId = $request->topic_id;
            $ranking = $request->ranking;

            // Update or create a preferred topic
            $preferred = PreferredTopic::updateOrCreate(
                [
                    'user_id' => $request->user()->id,
                    'topic_id' => $topicId
                ],
                ['ranking' => $ranking]
            );

            Log::info('Preferred topic saved successfully', ['preferred_topic_id' => $preferred->id]);

            return response()->json([
                'message' => __('Preferred topic saved successfully')
            ]);
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the preferred topic', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving preferred topic by ID', ['preferred_topic_id' => $id]);

            return response()->json(PreferredTopic::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve preferred topic by ID', [
                'preferred_topic_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        //
    }
}
