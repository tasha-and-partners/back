<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\AccountCreated;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Register user
     *
     */
    public function register(Request $request) {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'unique:users', 'email:rfc,dns'],
                'phone' => ['required', 'unique:users', 'string', 'max:15'],
                'password' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Registration validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $first_name = $request->first_name;
            $last_name = $request->last_name;
            $middle_name = $request->middle_name;
            $role = 'subscriber';
            $email = $request->email;
            $phone = $request->phone;
            $password = $request->password;

            $user = new User([
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'last_name' => $last_name,
                'avatar' => '',
                'role' => $role,
                'email' => $email,
                'phone' => $phone,
                'password' => Hash::make($password)
            ]);

            if ($user->save()) {
                Log::info('User registered successfully.', ['id' => $user->id]);

                // Send email to user with their password
                Mail::to($email)->send(new AccountCreated($first_name, $password));

                return response()->json($user);
            } else {
                Log::error('An error occurred while creating the user', ['email' => $request->email]);

                return response()->json(
                    [
                        'message' => __('An error occurred while creating the user! Please try again later.')
                    ],
                    500
                );
            }
        } catch (\Throwable $e) {
            Log::error('An error occurred while trying to register user', ['email' => $request->email]);

            return response()->json($e, 500);
        }
    }

    /**
     * Login admin user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminLogin(Request $request) {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            if (auth()->user()->role === 'admin') {
                try {
                    // Create user token
                    $token = auth()->user()->createToken(config('app.name'));

                    Log::info('User logged in successfully.', ['id' => auth()->user()->id]);

                    return response()->json(
                        [
                            'token' => $token->plainTextToken,
                            'user' => auth()->user()
                        ],
                        200
                    );
                } catch (\Throwable $e) {
                    Log::error('An error occurred when trying to login the user', ['error' => $e]);

                    return response()->json($e, 500);
                }
            } else {
                Log::warning('A non-admin user attempted to login to admin area.', ['email' => $request->email]);

                return response()->json(
                    ['message' => __('Unauthorized')],
                    403
                );
            }
        } else {
            Log::warning('User attempted to login with invalid credentials.', ['email' => $request->email]);

            return response()->json(
                [
                    'message' => __('Invalid email/password')
                ],
                401
            );
        }
    }

    /**
     * Login user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            try {
                // Create user token
                $token = auth()->user()->createToken(config('app.name'));

                Log::info('User logged in successfully.', ['id' => auth()->user()->id]);

                return response()->json(
                    [
                        'token' => $token->plainTextToken,
                        'user' => auth()->user()
                    ],
                    200
                );
            } catch (\Throwable $e) {
                Log::error('An error occurred when trying to login the user', ['error' => $e]);

                return response()->json($e, 500);
            }
        } else {
            Log::warning('User attempted to login with invalid credentials.', ['email' => $request->email]);

            return response()->json(
                [
                    'message' => __('Invalid email/password')
                ],
                401
            );
        }
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            // Delete the user's token
            auth()->user()->currentAccessToken()->delete();

            Log::info('Logged out user', ['id' => auth()->user()->id]);

            return response()->json(
                [
                    'success' => 'true',
                ],
                200
            );
        } catch (\Throwable $e) {
            Log::error('Failed to logout user', ['id', auth()->user()->id]);

            return response()->json($e, 500);
        }
    }
}
