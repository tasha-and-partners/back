<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $searchKey = $request->query('key');

            $topics = Topic::where(function ($query) use ($searchKey) {
                if ($searchKey != null) {
                    return $query->where('name', 'LIKE', '%' . $searchKey . '%');
                }
            })
            ->orderBy('name', 'asc')
            ->get();

            Log::info('Topics searched', [
                'topics' => $topics,
                'search_key' => $searchKey
            ]);

            return response()->json($topics);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for topics', [
                'error' => $e,
                'search_key' => $searchKey
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'description' => ['nullable', 'string']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving topic validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $name = $request->name;
            $description = $request->description;

            // Populate a topic object
            $topic = new Topic([
                'name' => $name,
                'description' => $description
            ]);

            // Save the topic and return a message
            if ($topic->save()) {
                Log::info('Topic saved successfully', ['topic_id' => $topic->id]);

                return response()->json([
                    'message' => __('Topic saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the topic');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the topic')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the topic', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving topic by ID', ['topic_id' => $id]);

            return response()->json(Topic::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve topic by ID', [
                'topic_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'description' => ['nullable', 'string']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating topic validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $name = $request->name;
            $description = $request->description;

            // Update topic
            $affected = Topic::where('id', $id)
                ->update([
                    'name' => $name,
                    'description' => $description
                ]);

            if ($affected) {
                Log::info('Topic updated successfully.', ['topic_id' => $id]);

                return response()->json(Topic::find($id));
            }

            Log::error('An error occurred when updating the topic');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the topic')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the topic', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the topic
            $topic = Topic::find($id);

            // Delete the topic
            $topic->delete();

            if ($topic->trashed()) {
                Log::info('Topic deleted successfully', ['topic_id' => $id]);

                return response()->json([
                    'message' => __('Topic deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the topic');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the topic')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the topic', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
