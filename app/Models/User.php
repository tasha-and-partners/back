<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'avatar',
        'role',
        'email',
        'phone',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be populated with the object.
     *
     * @var array
     */
    public $with = [
        'speaker',
        'moderator'
    ];


    /**
     * Get the user's speaker account
     */
    public function speaker()
    {
        return $this->hasOne(Speaker::class);
    }


    /**
     * Get the user's moderator account
     */
    public function moderator()
    {
        return $this->hasOne(Moderator::class);
    }


    /**
     * Get the user's survey question answers
     */
    public function answers()
    {
        return $this->hasMany(QuestionAnswer::class);
    }

    /**
     * Get the topics that the user prefers
     */
    public function topics()
    {
        return $this->belongsToMany(Topic::class, 'preferred_topics')->withPivot('ranking');
    }
}
