<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyQuestion extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id',
        'number',
        'content',
        'type',
        'choices'
    ];

    /**
     * Get the question's survey
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }


    /**
     * Get the survey question's answers
     */
    public function answers()
    {
        return $this->hasMany(QuestionAnswer::class);
    }
}
