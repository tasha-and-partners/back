<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Speaker extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'biography',
        'cv',
        'themes',
        'type'
    ];

    protected $primaryKey = 'user_id';

    /**
     * Relationships to attach to model
     */
    protected $with = [
        // 'user'
    ];

    /**
     * Get the speaker's user account
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
