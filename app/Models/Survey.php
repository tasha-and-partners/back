<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workshop_id',
        'title',
        'description',
        'status'
    ];

    /**
     * Get the survey's workshop
     */
    public function workshop()
    {
        return $this->belongsTo(Workshop::class);
    }


    /**
     * Get the survey's questions
     */
    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }
}
