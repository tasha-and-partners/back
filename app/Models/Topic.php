<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];


    /**
     * Get the topic's workshops
     */
    public function workshops()
    {
        return $this->hasMany(Workshop::class);
    }

    /**
     * Get the users who prefer this topic
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'preferred_topics')->withPivot('ranking');
    }
}
