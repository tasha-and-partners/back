<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Moderator extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'workshop_id'
    ];

    protected $primaryKey = 'user_id';

    /**
     * Relationships to attach to model
     */
    protected $with = [
        // 'user',
        'workshop'
    ];

    /**
     * Get the moderator's user account
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the moderator's workshop
     */
    public function workshop()
    {
        return $this->belongsTo(Workshop::class);
    }
}
