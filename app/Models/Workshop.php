<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workshop extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'speaker_id',
        'topic_id',
        'title',
        'outline',
        'session'
    ];

    /**
     * The attributes that should be populated with the object.
     *
     * @var array
     */
    public $with = [
        'speaker',
        'topic'
    ];


    /**
     * Get the workshop's speaker
     */
    public function speaker()
    {
        return $this->belongsTo(User::class, 'speaker_id');
    }


    /**
     * Get the workshop's topic
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }


    /**
     * Get the workshop's moderators
     */
    public function moderators()
    {
        return $this->hasMany(Moderator::class);
    }


    /**
     * Get the workshop's surveys
     */
    public function surveys()
    {
        return $this->hasMany(Survey::class);
    }
}
