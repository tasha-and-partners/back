<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferredTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferred_topics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('topic_id')->constrained('topics', 'id');
            $table->foreignId('user_id')->constrained('users', 'id');
            $table->tinyInteger('ranking');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferred_topics');
    }
}
